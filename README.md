# MySQL Replication (Percona)

1. В автоматическом режиме поднимается две машины 
```
master 192.168.11.150
slave  192.168.11.151
```
2. Устанавливается Percona Percona-Server-server-57 
3. На них копируются конфиги в папку /etc/my.cnf.d/
4. На slave вносятся корректировки в файлы  

```
/etc/my.cnf.d/01-basics.cnf server-id = 2                                       # меняем server ID
/etc/my.cnf.d/05-binlog.cnf replicate-ignore-table=bet.events_on_demand         # не принимаем данные из таблицы events_on_demand
/etc/my.cnf.d/05-binlog.cnf replicate-ignore-table=bet.v_same_event             # и из v_same_event
```
5. на обоих хостах запускается Percona Percona-Server-server-57
6. На этом Provision закончен.

# Настройки на Master

1. меняем автоматически сгенерированный пароль root
```
cat /var/log/mysqld.log | grep 'root@localhost:' | awk '{print $11}'
mysql -uroot -p'Generated_password'
mysql > ALTER USER USER() IDENTIFIED BY '!Otuslinux2020';
```
2. Заливаем базу из дампа 
```
mysql> CREATE DATABASE bet;
mysql -uroot -p -D bet < /opt/bet.dmp;
USE bet;
mysql> CREATE USER 'repl'@'%' IDENTIFIED BY '!OtusLinux2020';
```
3. Создаем дамп базы bet для переноса на slave
```
cd /opt/
mysqldump --all-databases --triggers --routines --master-data --ignore-table=bet.events_on_demand --ignore-table=bet.v_same_event -uroot -p > master.sql
```
- Явно указываем какие таблицы не будем включать в дамп `--ignore-table=bet.events_on_demand --ignore-table=bet.v_same_event`
 
4. Узнаем MASTER_LOG_FILE и MASTER_LOG_POS для настроек репликации на slave
```
mysql> SHOW MASTER STATUS;
MASTER_LOG_FILE = File    
MASTER_LOG_POS = position
```

# Настройки на Slave

1. Меняем автоматически сгенерированный пароль root
```
cat /var/log/mysqld.log | grep 'root@localhost:' | awk '{print $11}'
mysql -uroot -p'Generated_password'
mysql > ALTER USER USER() IDENTIFIED BY '!Otuslinux2020';
```
2. Копируем дамп с master 
`scp root@192.168.11.150:/opt/master.sql /opt/master.sql`
3. Заливаем дамп 
```
mysql> SOURCE /opt/master.sql
USE bet;
```
4. Запускаем репликацию используя полученные из настроек мастера MASTER_LOG_FILE и MASTER_LOG_POS
```
mysql> CHANGE MASTER TO MASTER_HOST = "192.168.11.150", MASTER_PORT = 3306, MASTER_USER = "repl", MASTER_PASSWORD = "!OtusLinux2018", MASTER_LOG_FILE='mysql-bin.000002',MASTER_LOG_POS=119568;
mysql> START SLAVE;
```
* Предложенный вариант в методичке  `MASTER_AUTO_POSITION = 1;` не сработал, были использованы `MASTER_LOG_FILE и MASTER_LOG_POS`
5. Для проверки запускаем комманду `SHOW SLAVE STATUS\G`

Эти строки указывают на корректность работы. Дополнительно скрин в папке screenshots.
```
Slave_IO_Running: Yes
Slave_SQL_Running: Yes
```

# Проверка 

1. На master добавляем запись в таблицу bookmaker
```
mysql> USE bet;
mysql> INSERT INTO bookmaker (id,bookmaker_name) VALUES(1,'1xbet');
```
2. На slave 
```
mysql> USE bet;
SELECT * FROM bookmaker;
SHOW TABLES;
```

Как видно из скриншота -  реплицировались только необходимые таблицы и в табл. bookmaker добавилась запись 1xbet.
